.head 0 +  Application Description: SHELL.APL

Entwickler: J�rg Ellinghaus
EMail: ELLI@ONLINE-CLUB.DE

Beschreibung: 
Funktionsklasse fcShell, die externe Programme oder Dokumente aufruft.
Wird nicht behandelt wie SalLoadApp().

Update:
30.03.1999	- On WIN-NT the last version crashes, because of the settings for ShellExecuteA()
.head 1 -  Outline Version - 4.0.27
.head 1 +  Design-time Settings
.data VIEWINFO
0000: 6F00000001000000 FFFF01000D004347 5458566965775374 6174650400010000
0020: 0000000000A50000 002C000000020000 0003000000FFFFFF FFFFFFFFFFFCFFFF
0040: FFE4FFFFFFFFFFFF FF000000007C0200 004D010000010000 0000000000010000
0060: 000F4170706C6963 6174696F6E497465 6D00000000
.enddata
.head 2 -  Outline Window State: Normal
.head 2 +  Outline Window Location and Size
.data VIEWINFO
0000: 6600040003002D00 0000000000000000 0000B71E5D0E0500 1D00FFFF4D61696E
0020: 0000000000000000 0000000000000000 0000003B00010000 00000000000000E9
0040: 1E800A00008600FF FF496E7465726E61 6C2046756E637469 6F6E730000000000
0060: 0000000000000000 0000000000003200 0100000000000000 0000E91E800A0000
0080: DF00FFFF56617269 61626C6573000000 0000000000000000 0000000000000000
00A0: 3000010000000000 00000000F51E100D 0000F400FFFF436C 6173736573000000
00C0: 0000000000000000 0000000000000000
.enddata
.data VIEWSIZE
0000: D000
.enddata
.head 3 -  Left:   -0.013"
.head 3 -  Top:    0.0"
.head 3 -  Width:  8.013"
.head 3 -  Height: 4.969"
.head 2 +  Options Box Location
.data VIEWINFO
0000: D4180909B80B1A00
.enddata
.data VIEWSIZE
0000: 0800
.enddata
.head 3 -  Visible? No
.head 3 -  Left:   4.15"
.head 3 -  Top:    1.885"
.head 3 -  Width:  3.8"
.head 3 -  Height: 2.073"
.head 2 +  Class Editor Location
.head 3 -  Visible? No
.head 3 -  Left:   0.575"
.head 3 -  Top:    0.094"
.head 3 -  Width:  5.063"
.head 3 -  Height: 2.719"
.head 2 +  Tool Palette Location
.head 3 -  Visible? No
.head 3 -  Left:   6.388"
.head 3 -  Top:    0.729"
.head 2 -  Fully Qualified External References? Yes
.head 2 -  Reject Multiple Window Instances? No
.head 2 -  Enable Runtime Checks Of External References? Yes
.head 2 -  Use Release 4.0 Scope Rules? No
.head 1 -  Libraries
.head 1 +  Global Declarations
.head 2 +  Window Defaults
.head 3 +  Tool Bar
.head 4 -  Display Style? Etched
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Form Window
.head 4 -  Display Style? Etched
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Dialog Box
.head 4 -  Display Style? Etched
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Top Level Table Window
.head 4 -  Font Name: MS Sans Serif
.head 4 -  Font Size: 8
.head 4 -  Font Enhancement: System Default
.head 4 -  Text Color: System Default
.head 4 -  Background Color: System Default
.head 3 +  Data Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Multiline Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Spin Field
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Background Text
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Pushbutton
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Radio Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Check Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Option Button
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 3 +  Group Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Child Table Window
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  List Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Combo Box
.head 4 -  Font Name: Use Parent
.head 4 -  Font Size: Use Parent
.head 4 -  Font Enhancement: Use Parent
.head 4 -  Text Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 3 +  Line
.head 4 -  Line Color: Use Parent
.head 3 +  Frame
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: 3D Face Color
.head 3 +  Picture
.head 4 -  Border Color: Use Parent
.head 4 -  Background Color: Use Parent
.head 2 +  Formats
.head 3 -  Number: 0'%'
.head 3 -  Number: #0
.head 3 -  Number: ###000
.head 3 -  Number: ###000;'($'###000')'
.head 3 -  Date/Time: hh:mm:ss AMPM
.head 3 -  Date/Time: M/d/yy
.head 3 -  Date/Time: MM-dd-yy
.head 3 -  Date/Time: dd-MMM-yyyy
.head 3 -  Date/Time: MMM d, yyyy
.head 3 -  Date/Time: MMM d, yyyy hh:mm AMPM
.head 3 -  Date/Time: MMMM d, yyyy hh:mm AMPM
.head 2 +  External Functions
.head 3 +  Library name: shell32.dll
.head 4 +  Function: ShellExecuteA
.head 5 -  Description: ShellExecute �ffnet oder druckt eine Datei. Die Datei kann eine ausf�hrbare Datei oder ein Dokument sein.

param1 = Window Handle das Parent Window, das die Messages (Messagebox bei einem Fehler) erh�lt
param2 = Pointer der Operation 'open', 'print', 'explore'
param3 = Die Datei (Name + Suffix)
param4 = Die Parameter bei EXE-Datei ansonsten Null
param5 = Der Pfad
param6 = SW_* Windows-Konstante zur entsprechenden Anzeigeart
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 +  Parameters
.head 6 -  Window Handle: HWND
.head 6 -  String: LPCSTR
.head 6 -  String: LPCSTR
.head 6 -  String: LPCSTR
.head 6 -  String: LPCSTR
.head 6 -  Number: INT
.head 4 +  Function: FindExecutableA
.head 5 -  Description: FindExecutableA sucht nach der angegebenen EXE-Datei in den Processen und gibt die HINSTANCE zur�ck falls vorhanden.
Funcktioniert nur mit ausf�hrbaren Dateien.

param1 = Die EXE-Datei (Name + Suffix)
param2 = Der Pfad
param3 = ''
.head 5 -  Export Ordinal: 0
.head 5 +  Returns
.head 6 -  Number: DWORD
.head 5 +  Parameters
.head 6 -  String: LPCSTR
.head 6 -  String: LPCSTR
.head 6 -  String: LPSTR
.head 2 +  Constants
.data CCDATA
0000: 3000000000000000 0000000000000000 00000000
.enddata
.data CCSIZE
0000: 1400
.enddata
.head 3 +  System
.head 4 -  Number: SW_SHOWNORMAL = 1
.head 4 -  Number: SW_SHOWMINIMIZED = 2
.head 4 -  Number: SW_SHOWMAXIMIZED = 3
.head 4 -  Number: SW_SHOWNOACTIVATE = 4
.head 4 -  Number: SW_SHOW = 5
.head 4 -  Number: SW_MINIMIZE = 6
.head 4 -  Number: SW_SHOWMINNOACTIVE = 7
.head 4 -  Number: SW_SHOWNA = 8
.head 4 -  Number: SW_RESTORE = 9
.head 4 -  Number: SW_SHOWDEFAULT = 10
.head 3 -  User
.head 2 -  Resources
.head 2 -  Variables
.head 2 -  Internal Functions
.head 2 -  Named Menus
.head 2 +  Class Definitions
.head 3 +  Functional Class: fcShell
.head 4 -  Description: Zugriff auf Shellfunktionen

Funnktionsklasse wird durch Oeffnen( ... ) aufgerufen und f�hrt dann ein ShellExecute aus.
Es kann auch verhindert werden (durch den letzten Parameter), da� das Programm mehrfach ge�ffnet wird.

Beispiel: 
1. �ffnen einer EXE: CShell.Oeffnen( hWndNULL, 'c:\\windows\\notepad.exe', 'c:\\temp\\test.txt', 'open', 0,0 )
2. �ffnen eines Internet-Dokuments: CShell.Oeffnen( hWndNULL, 'c:\\daten\\html\\home.htm', '', 'open', 0, 0 )
3. �ffnen eines Word-Dokuments: CShell.Oeffnen( hWndNULL, 'c:\\daten\\word\\wichtig.doc', '', 'open', 0, 0 )
.head 4 -  Derived From
.head 4 -  Class Variables
.head 4 +  Instance Variables
.head 5 -  Window Handle: whFenster		! Window Handle das Fehlermeldungen erh�lt.
.head 5 -  String: sDatei			! Dateiname
.head 5 -  String: sSuffix			! Suffix der DAtei
.head 5 -  String: sPfad			! Pfad
.head 5 -  String: sParameter			! Parameter
.head 5 -  String: sOperation			! Operation 'open', 'print', 'explore'
.head 5 -  Number: nFensterart		! Windows SW_* Option
.head 5 -  Boolean: bMehrfach		! Programm ist nur einmal zu �ffnen oder mehrfach. Default ist nur einmal FALSE (nur bei EXE)
.head 5 -  Number: hShell			! Return von HINSTANCE
.head 4 +  Functions
.head 5 +  Function: Aufrufen
.head 6 -  Description:
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  Window Handle: whGeoeffnet
.head 6 +  Actions
.head 7 -  !
.head 7 +  If ( sSuffix = 'EXE' or sSuffix = 'COM' ) and Not bMehrfach
.head 8 -  Set whGeoeffnet = SalAppFind( sDatei, TRUE )
.head 8 +  If whGeoeffnet = hWndNULL
.head 9 -  Set hShell = ShellExecuteA( whFenster, sOperation, sDatei, sParameter, sPfad, nFensterart )
.head 7 +  Else
.head 8 -  Set hShell = ShellExecuteA( whFenster, sOperation, sDatei, sParameter, sPfad, nFensterart )
.head 5 +  Function: HoleDateityp
.head 6 -  Description: Dateityp herausfiltern
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: saTemp[*]
.head 7 -  Number: nAnzahl
.head 6 +  Actions
.head 7 +  If sDatei = ''
.head 8 -  Return FALSE
.head 7 -  !
.head 7 -  Set nAnzahl = SalStrTokenize( sDatei, '.', '.', saTemp )
.head 7 +  If nAnzahl > 1
.head 8 -  Set sSuffix = SalStrUpperX( saTemp[ nAnzahl - 1 ] )
.head 7 -  !
.head 7 +  If sSuffix = ''
.head 8 -  Set sSuffix = 'EXE'
.head 8 -  Set sDatei = sDatei || '.EXE'
.head 7 -  ! !
.head 7 +  If sSuffix != 'EXE' and sSuffix != 'COM'
.head 8 -  Set bMehrfach = FALSE
.head 5 +  Function: HolePfad
.head 6 -  Description: Pfadangabe herausfiltern
.head 6 -  Returns
.head 6 -  Parameters
.head 6 -  Static Variables
.head 6 +  Local variables
.head 7 -  String: saTemp[*]
.head 7 -  Number: nAnzahl
.head 7 -  Number: nIndex
.head 6 +  Actions
.head 7 +  If sDatei = ''
.head 8 -  Return FALSE
.head 7 -  !
.head 7 -  Set nAnzahl = SalStrTokenize( sDatei, '\\', '\\',  saTemp )
.head 7 +  If nAnzahl > 1
.head 8 -  Set nIndex = 0
.head 8 +  Loop
.head 9 -  Set sPfad = sPfad || '\\' || saTemp[ nIndex ]
.head 9 -  Set nIndex = nIndex + 1
.head 9 +  If nIndex = nAnzahl - 1
.head 10 -  Break
.head 8 -  Call SalStrLop( sPfad )
.head 5 +  Function: Oeffnen
.head 6 -  Description: Initialisiert die entsprechenden Variablen und ruft die Shell auf
.head 6 -  Returns
.head 6 +  Parameters
.head 7 -  Window Handle: whpFenster
.head 7 -  String: spDatei
.head 7 -  String: spParameter
.head 7 -  String: spOperation
.head 7 -  Number: npFenster
.head 7 -  Boolean: bpMehrfach
.head 6 -  Static Variables
.head 6 -  Local variables
.head 6 +  Actions
.head 7 -  Set whFenster = whpFenster
.head 7 -  Set sDatei = spDatei
.head 7 -  Set sParameter = spParameter
.head 7 -  Set sOperation = spOperation
.head 7 -  Set nFensterart = npFenster
.head 7 -  !
.head 7 +  If sOperation = ''
.head 8 -  Set sOperation = 'open'
.head 7 -  !
.head 7 +  If nFensterart = 0
.head 8 -  Set nFensterart = SW_SHOWNORMAL
.head 7 -  !
.head 7 -  Call HoleDateityp()
.head 7 -  !
.head 7 -  Call HolePfad()
.head 7 -  !
.head 7 -  Call Aufrufen()
.head 2 +  Default Classes
.head 3 -  MDI Window: cBaseMDI
.head 3 -  Form Window:
.head 3 -  Dialog Box:
.head 3 -  Table Window:
.head 3 -  Quest Window:
.head 3 -  Data Field:
.head 3 -  Spin Field:
.head 3 -  Multiline Field:
.head 3 -  Pushbutton:
.head 3 -  Radio Button:
.head 3 -  Option Button:
.head 3 -  ActiveX:
.head 3 -  Check Box:
.head 3 -  Child Table:
.head 3 -  Quest Child Window: cQuickDatabase
.head 3 -  List Box:
.head 3 -  Combo Box:
.head 3 -  Picture:
.head 3 -  Vertical Scroll Bar:
.head 3 -  Horizontal Scroll Bar:
.head 3 -  Column:
.head 3 -  Background Text:
.head 3 -  Group Box:
.head 3 -  Line:
.head 3 -  Frame:
.head 3 -  Custom Control:
.head 2 -  Application Actions
