CREATE OR REPLACE VIEW V_DECKELAUSZUG
(idliste, listencode,liste_erfasst,header_bemerkung, gruppierung, produktegruppe, refnr, objekt, pos, sdvariante, geraetetyp, ablech_material, ablech_beschichtung, iblech_material, iblech_beschichtung, verschluss, scharnier, schraube, buechse, isolation, desc_bemerkung, idpos, pos_sortierung, pos_menge, pos_produkt, pos_verschluss, pos_nennmass_x, pos_nennmass_y, pos_bemerkung, idoption, opt_option, opt_coord_x, opt_coord_y, opt_ablech, opt_iblech)
AS
SELECT
 a.id,
 to_char(a.erf_datum,'yyyymmdd') || ' - Liste#' || a.listennummer,
 a.erf_id || '  (Host: ' || a.erf_host || ')',
 a.bemerkung,
 b.iddescription || decode(b.bodenblech,1,e4.BLECH,DECODE(d.wanne,1,e3.blech,e2.BLECH)) ||
 decode(b.bodenblech,1,nvl(c.i_bodenbeschichtet,0),0) || decode(d.wanne,0,nvl(c.i_wannebeschichtet,0),0),
 d.druck_gruppierung,
 blech.pkg_description.fget_refnr(a.idtermin),
 CAST(blech.pkg_description.fget_object(a.idtermin) AS VARCHAR2(50 CHAR)),
 CAST(blech.pkg_description.fget_apos(a.idtermin) AS VARCHAR2(10 CHAR)) || ' ' || decode(nvl(c.atex,0),0,null,'EX'),
 v.variante,
 CAST(blech.pkg_description.fget_gtyp(a.idtermin) AS VARCHAR2(10 CHAR)),
 e1.BLECH || decode(nvl(c.a_idikell,0),0,NULL,' (Idikell)'),
 cast(decode(nvl(c.a_uv,0),0,null,'PES.') || blech.pkg_description.fget_beschichtung (c.a_idfarbe,
                                                   c.a_idfarbstruktur,
                                                   c.a_idglanzgrad,
                                                   h1.schichtdicke,
                                                   0,b.idverbohrung)  as varchar2(100 char)) ,
 DECODE(b.bodenblech,1,e4.BLECH,DECODE(d.wanne,1,e3.blech,e2.blech)) || decode(nvl(c.i_idikell,0),0,NULL,' (Idikell)'),
 decode(b.bodenblech,1,
  decode(nvl(c.i_bodenbeschichtet,0),0,cast(blech.pkg_description.fget_beschichtung (c.i_idfarbe,
                                                   c.i_idfarbstruktur,
                                                   c.i_idglanzgrad,
                                                   h2.schichtdicke,
                                                   1,b.idverbohrung)  as varchar2(100 char)),'.nicht beschichtet'),
  decode(d.wanne,1,
   decode(nvl(c.i_wannebeschichtet,0),0,cast(blech.pkg_description.fget_beschichtung (c.i_idfarbe,
                                                   c.i_idfarbstruktur,
                                                   c.i_idglanzgrad,
                                                   h2.schichtdicke,
                                                   1,b.idverbohrung)  as varchar2(100 char)),'.nicht beschichtet'),
  cast(blech.pkg_description.fget_beschichtung (c.i_idfarbe,
                                                   c.i_idfarbstruktur,
                                                   c.i_idglanzgrad,
                                                   h2.schichtdicke,
                                                   1,b.idverbohrung)  as varchar2(100 char)))),
 cast(blech.pkg_description.fget_zusaetze (c.idverschluss, 1,b.idverbohrung, b.bodenblech) as varchar2(25 char)),
 cast(blech.pkg_description.fget_zusaetze (c.idscharnier, 2,b.idverbohrung, b.bodenblech) as varchar2(25 char)),
 cast(blech.pkg_description.fget_zusaetze (c.idschraube, 3,b.idverbohrung, b.bodenblech) as varchar2(25 char)),
 decode(c.buechse,1,'beschichtet','nicht beschichtet'),
 cast(blech.pkg_description.fget_zusaetze (c.idisolation, 4,b.idverbohrung, b.bodenblech) as varchar2(50 char)),
 c.bemerkung,
 b.id,
 lpad(b.nennmass_breite_x,5,'0') || LPAD(b.nennmass_hoehe_y,5,0) || LPAD(b.id,10,0),
 b.menge,
 decode(nvl(c.thermo,0),0,null,decode(d.wanne,1,null,'T-')) || CAST(blech.pkg_description.fget_diff_prodtyp(a.idtermin,c.idptyp) AS VARCHAR2(10 CHAR)) || replace(d.typ,'&','') ||
  decode(nvl(b.idverschluss,0),0,NULL,
   decode(nvl(b.stk_scharnier,0),0,null,
    decode( b.revtuere_band_links,1,'/L S=','/R S=') || b.stk_scharnier || '/')) ||
  decode(nvl(b.stk_verschluss_y,0),0,null,' V(y)=' ||b.stk_verschluss_y) ||
  decode(nvl(b.stk_verschluss_x,0),0,NULL,' V(x)=' || b.stk_verschluss_x),
 decode(nvl(b.idverschluss,0),0,null,vt.verschluss) || decode(nvl(b.idverbohrung,0),0,null,g1.listensymbol) ,
 b.nennmass_breite_x,
 b.nennmass_hoehe_y,
 b.bemerkung,
 F.ID,
 cast(decode(f1.zusatz,null,null,
  decode(f.ablech,1,decode(nvl(f.iblech,0),1,null,'(A) '),
    decode(nvl(f.iblech,0),1,decode(nvl(f.ablech,0),1,null,'(I) '))) ||
  f1.zusatz || ' ' || decode(nvl(f.idvprofil,0),0,null,g.code) || '   ' ||
  blech.pkg_transform.Fget_Label_Liste(f.id,f1.label_liste)) || decode(vt1.verschluss,null,null,'  .' || vt1.verschluss) as varchar2(150 CHAR)) ,
 decode(nvl(f.par_x,0),0,null,f.par_x),
 decode(nvl(f.par_y,0),0,null,f.par_y),
 f.ablech,
 f.iblech
FROM
 blech.deckel_header a,
 blech.deckel_pos b,
 blech.deckel_description c,
 blech.adm_deckeltypen d,
 blech.v_adm_bleche e1,
 blech.v_adm_bleche e2,
 blech.v_adm_bleche e3,
 blech.v_adm_bleche e4,
 blech.deckel_optionen f,
 blech.adm_optionen f1,
 blech.adm_optionengruppen f2,
 blech.v_vprofile_ptyp_code g,
 blech.adm_varianten v,
 blech.adm_verschlusstypen vt,
 blech.adm_verschlusstypen vt1,
 blech.adm_farbe_schichtdicke h1,
 blech.adm_farbe_schichtdicke h2,
 blech.adm_verbohrungen g1,
 blech.adm_druckstufen g2
WHERE
 b.iddeckelheader = a.id AND
 c.id = b.iddescription AND
 v.id = c.idvariante and
 d.id = b.iddeckelart AND
 e1.ID = c.a_idblech AND
 e2.ID = c.i_idblech AND
 e3.id = c.i_idblechwanne AND
 e4.ID = c.i_idblechboden AND
 f.iddeckelpos(+) = b.id AND
 f1.id(+) = f.idoption AND
 f2.id(+) = f1.idoptionengruppe and
 vt.id(+) = b.idverschluss and
 vt1.id(+) = f.idverschluss and
 g.id(+) = f.idvprofil and
 h1.id(+) = c.a_idschichtdicke and
 h2.id(+) = c.i_idschichtdicke and
 g1.id(+) = b.idverbohrung and
 g2.id(+) = g1.iddruckstufe;
