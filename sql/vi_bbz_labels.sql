CREATE OR REPLACE VIEW BLECH.VI_BBZ_LABELS
(aussen, seqnr, refnr_pos, ablech, iblech, dimension)
AS
SELECT
        1,
        a.id,
        cast(d.refnr || '.' || d.pos || '  ' || BLECH.FGET_INFO_OPTIONEN( a.id ) as varchar2(20 char)) ,
        f1.werkstoff || '-' || h1.farbe,
 cast( blech.fget_iblech(p_nidpos => a.id) as varchar2(50 char)),
        a.nennmass_breite_x || 'x' || a.nennmass_hoehe_y
    FROM
        blech.deckel_pos a,
        blech.deckel_header b,
        sevprod.a_termin c,
        sevprod.a_pos_allg d,
        blech.deckel_description e1,
        blech.v_adm_bleche f1,
        blech.v_adm_bleche f2,
        blech.v_adm_bleche f3,
        blech.v_adm_bleche f4,
        blech.adm_deckeltypen g1,
        global.adm_farbe h1,
        global.adm_farbe h2
    WHERE
        b.id = a.iddeckelheader AND
        c.at_seqnr = b.idtermin AND
        d.p_seqnr = c.p_seqnr AND
        E1.ID = A.IDDESCRIPTION AND
        f1.id = e1.a_idblech AND
        f2.id = e1.i_idblech AND
        f3.id = e1.i_idblechboden AND
        f4.id = e1.i_idblechwanne AND
        g1.id = a.iddeckelart AND
        h1.id (+) = e1.a_idfarbe AND
        h2.id (+) = e1.i_idfarbe
    UNION
        SELECT
            0,
            a.id,
            cast(d.refnr || '.' || d.pos || '  ' || BLECH.FGET_INFO_OPTIONEN( a.id ) as varchar2(20char)),
            NULL,
   cast(blech.fget_iblech(p_nidpos => a.id) as varchar2(50 char)),

            a.nennmass_breite_x || 'x' || a.nennmass_hoehe_y
        FROM
            blech.deckel_pos a,
            blech.deckel_header b,
            sevprod.a_termin c,
            sevprod.a_pos_allg d,
            blech.deckel_description e1,
            blech.v_adm_bleche f1,
            blech.v_adm_bleche f2,
            blech.v_adm_bleche f3,
            blech.v_adm_bleche f4,
            blech.adm_deckeltypen g1,
            global.adm_farbe h1,
            global.adm_farbe h2
        WHERE
            b.id = a.iddeckelheader AND
            c.at_seqnr = b.idtermin AND
            d.p_seqnr = c.p_seqnr AND
            E1.ID = A.IDDESCRIPTION AND
            f1.id = e1.a_idblech AND
            f2.id = e1.i_idblech AND
            f3.id = e1.i_idblechboden AND
            f4.id = e1.i_idblechwanne AND
            g1.id = a.iddeckelart AND
            h1.id (+) = e1.a_idfarbe AND
            h2.id (+) = e1.i_idfarbe;
