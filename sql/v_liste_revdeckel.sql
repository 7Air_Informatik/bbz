create or replace view v_liste_revdeckel
(idheader, idkwjahr, jahr, woche, schub, objekt, menge, produktgruppe, only_produkt, produkt, dimension, verschlusstyp, verschluss, scharnier, schrauben, buechse, aussenblech, innenblech,idzusatz, zusatz, xy_achse)
as
select
   a.id,
   cast(a.prod_jahr || lpad(a.prod_woche,2,0) as number),
   a.prod_jahr,
   a.prod_woche,
   a.prod_schub,
   k2.refnr ||'.' || k2.pos || ' - ' || k3.objekt,
   b1.menge,
  CAST(a.id || replace(b2.typ,'&','') || b1.nennmass_breite_x || 'x' || b1.nennmass_hoehe_y AS VARCHAR2(50 CHAR)),
  replace(b2.typ,'&',''),
  CAST(replace(b2.typ,'&','') ||
   decode(nvl(b1.stk_scharnier,0),0,null,decode(b1.revtuere_band_links,1,'Band=L S=','Band=R S=') || b1.stk_scharnier || '/') ||
   '  V(y)=' ||b1.stk_verschluss_y || '- V(x)=' || b1.stk_verschluss_x AS VARCHAR2(150 CHAR)),
   b1.nennmass_breite_x || ' x ' || b1.nennmass_hoehe_y,
   i.verschluss,
   j1.material,
   j2.material,
   j3.material,
   decode(nvl(d.buechse,0),0,'normal','beschichtet'),
   'A: ' || e1.werkstoff || '(' || f1.farbe || ' ' || g1.schichtdicke || '-'|| h1.glanzgrad ||')',
   'I: ' || decode(b1.bodenblech,e3.werkstoff,decode(b2.wanne,1,e4.WERKSTOFF,e2.WERKSTOFF)) ||
       '(' || f2.farbe || ' ' || g2.schichtdicke || '-' || h2.glanzgrad || ')',
   c.idoption,
   c1.zusatz || ' (' || decode(nvl(c.par_r,0),0,null,' r = ' || c.par_r) || ' ' ||
             decode(nvl(c.par_u,0),0,null,' u = ' || c.par_u)  || ' ' ||
             decode(nvl(c.par_v,0),0,null,' v = ' || c.par_v) || ')',
   DECODE(NVL(c.par_x,0),0,DECODE(NVL(C.PAR_Y,0),0,NULL,'XY-Achse: ')) ||
             DECODE(NVL(c.par_x,0),0,NULL,decode(nvl(c1.axis_over_par_h,0),0,' x = ',decode(nvl(c.par_h,1),1,' x = ',3,' x = ',' y = ')) || c.par_x ) ||
             DECODE(NVL(c.par_y,0),0,NULL,' y = ' || c.par_y )
from
   blech.deckel_header a,
   blech.deckel_pos b1,
   blech.adm_deckeltypen b2,
   blech.deckel_optionen c,
   blech.adm_optionen c1,
   blech.deckel_description d,
   blech.v_adm_bleche e1,
   blech.v_adm_bleche e2,
   blech.v_adm_bleche e3,
   blech.v_adm_bleche e4,
   global.adm_farbe f1,
   global.adm_farbe f2,
   blech.adm_farbe_schichtdicke g1,
   blech.adm_farbe_schichtdicke g2,
   blech.adm_glanzgrad h1,
   blech.adm_glanzgrad h2,
   blech.adm_verschlusstypen i,
   blech.adm_verschluss j1,
   blech.adm_scharniere j2,
   blech.adm_schrauben j3,
   sevprod.a_termin k1,
   sevprod.a_pos_allg k2,
   sevprod.a_kopf k3
where
   b1.iddeckelheader = a.id and
   b2.id = b1.iddeckelart and
   c.iddeckelpos(+) = b1.id and
   c1.id(+) = c.idoption and
   d.id = b1.iddescription and
   e1.ID = d.a_idblech and
   e2.ID = d.i_idblech and
   e3.id = d.i_idblechboden and
   e4.ID = d.i_idblechwanne and
   f1.id = d.a_idfarbe and
   f2.id = d.i_idfarbe and
   g1.id = d.a_idschichtdicke and
   g2.id = d.i_idschichtdicke and
   h1.id = d.a_idglanzgrad and
   h2.id = d.i_idglanzgrad and
   i.id = b1.idverschluss and
   j1.id = d.idverschluss and
   j2.id = d.idscharnier and
   j3.id = d.idschraube and
   k1.at_seqnr = a.idtermin and
   k2.p_seqnr = k1.p_seqnr and
   k3.refnr = k2.refnr;
