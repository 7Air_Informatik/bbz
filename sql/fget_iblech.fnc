CREATE OR REPLACE FUNCTION blech.fget_iblech(p_nidpos IN NUMBER)
  RETURN STRING IS
  sreturn VARCHAR2(50 CHAR);

  vwanne            NUMBER;
  vboden            NUMBER;
  vidfarbe          NUMBER;
  vfarbe            VARCHAR2(50 CHAR);
  vschichtdicke     VARCHAR2(25 CHAR);
  vidschichtdicke   NUMBER;
  vpes              VARCHAR2(5 CHAR);
  vwannebeschichtet NUMBER;
  vbodenbeschichtet NUMBER;
  vidmaterial       NUMBER;
  vidmaterialboden  NUMBER;
  vidmaterialwanne  NUMBER;
  vmaterial         VARCHAR2(5 CHAR);

  CURSOR cur_produkt IS
    SELECT t.wanne
      FROM blech.adm_deckeltypen t
     WHERE t.id = (SELECT p.iddeckelart
                     FROM blech.deckel_pos p
                    WHERE p.id = p_nidpos);

  CURSOR cur_description IS
    SELECT t.i_idfarbe,
           decode(t.a_uv, 1, 'PES.', NULL),
           t.i_idschichtdicke,
           t.i_wannebeschichtet,
           t.i_bodenbeschichtet,
           t.i_idblech,
           t.i_idblechboden,
           t.i_idblechwanne
      FROM blech.deckel_description t
     WHERE t.id = (SELECT p.iddescription
                     FROM blech.deckel_pos p
                    WHERE p.id = p_nidpos);

  CURSOR cur_color IS
    SELECT t.farbnummer || ' ' || t.farbe
      FROM global.adm_farbe t
     WHERE t.id = vidfarbe;

  CURSOR cur_floor IS
    SELECT t.bodenblech FROM blech.deckel_pos t WHERE t.id = p_nidpos;

  CURSOR cur_material IS
    SELECT t.werkstoff FROM blech.v_adm_bleche t WHERE t.id = vidmaterial;

  CURSOR cur_schichtdicke IS
    SELECT t.schichtdicke
      FROM blech.adm_farbe_schichtdicke t
     WHERE t.id = vidschichtdicke
       AND t.etikett = 1;

BEGIN

  sreturn := '.nicht beschichtet';
  -- Eigenschaften des Deckels werden ermittelt
  OPEN cur_description;
  FETCH cur_description
    INTO vidfarbe,
         vpes,
         vidschichtdicke,
         vwannebeschichtet,
         vbodenbeschichtet,
         vidmaterial,
         vidmaterialboden,
         vidmaterialwanne;
  CLOSE cur_description;

  -- Ist das Produkt eine Wanne?
  OPEN cur_produkt;
  FETCH cur_produkt
    INTO vwanne;
  CLOSE cur_produkt;

  -- Ist das Produkt ein Bodenblech?
  OPEN cur_floor;
  FETCH cur_floor
    INTO vboden;
  CLOSE cur_floor;

  IF vwanne = 1
  THEN
    vidmaterial := vidmaterialwanne;
  ELSIF vboden = 1
  THEN
    vidmaterial := vidmaterialboden;
  END IF;

  OPEN cur_material;
  FETCH cur_material
    INTO vmaterial;
  CLOSE cur_material;

  IF vidfarbe >= 1
  THEN
    -- Farbe wird ermittelt
    OPEN cur_color;
    FETCH cur_color
      INTO vfarbe;
    CLOSE cur_color;
  
    -- Schichtdicke wird ermittelt
    OPEN cur_schichtdicke;
    FETCH cur_schichtdicke
      INTO vschichtdicke;
    CLOSE cur_schichtdicke;
  
    IF vwanne = 1
    THEN
      -- Wanne
      IF vwannebeschichtet = 1
      THEN
        sreturn := vpes || vfarbe || ' ' || vschichtdicke;
      END IF;
    ELSE
      IF vboden = 1
      THEN
        -- Bodenblech
        IF vbodenbeschichtet = 1
        THEN
          sreturn := vpes || vfarbe || ' ' || vschichtdicke;
        END IF;
      ELSE
        -- normale Paneele
        sreturn := vpes || vfarbe || ' ' || vschichtdicke;
        NULL;
      END IF;
    END IF;
  END IF;

  sreturn := vmaterial || ' - ' || sreturn;
  RETURN sreturn;

END;
/
