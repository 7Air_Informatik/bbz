create or replace view v_deckeloptionen
(iddeckelpos, id, idoption, zusatz, coord_x, coord_y, radius_r, breite_u, hoehe_v, funktion, hwnd, label, label_text, par_l, par_n, par_q, par_s, par_o, par_k, par_m, par_h, par_w, vprofil, verschl_draw_typ, verschl_draw_color_rgb, verschl_draw_color_color)
as
select
    A.IDDECKELPOS,
    A.ID,
    A.IDOPTION,
    decode(nvl(a.ablech,0),0,decode(nvl(a.iblech,0),0,null,'I-'),decode(nvl(a.iblech,0),0,'A-',null)) || B.ZUSATZ || decode(nvl(a.idvprofil,0),0,null,' ' || d.code) || 
    decode(e1.verschluss,null,null,'.' || e1.verschluss),
    A.PAR_X,
    A.PAR_Y,
    A.PAR_R,
    A.PAR_U,
    A.PAR_V,
    c.cad_subroutine,
    B.HWND,
    decode(nvl(a.idvprofil,0),0,b.label,1),
    decode(nvl(a.idvprofil,0),0,b.label_text,d.code),
    a.par_l,
    a.par_n,
    a.par_q,
    a.par_s,
    a.par_o,
    a.par_k,
    a.par_m,
    a.par_h,
    a.par_w,
    d.code,
    e1.draw_typ,
    e2.code_rgb,
    e2.farbe
from
    BLECH.DECKEL_OPTIONEN a,
    BLECH.ADM_OPTIONEN b,
    blech.adm_option_steuerung b1,
    blech.adm_prg_cadsteuerung c,
    blech.v_vprofile_ptyp_code d,
    blech.adm_verschlusstypen e1,
    global.adm_farbtabelle e2
where
    B.ID = A.IDOPTION and
    b1.id(+) = b.id_inc_steuerung and
    c.id = b.idprg_cadfunktion and
    d.id(+) = a.idvprofil and
    e1.id(+) = a.idverschluss and
    e2.id(+) = e1.idcolor
order by a.erf_datum desc;
