CREATE OR REPLACE VIEW BLECH.V_USED_MATERIAL
(
    IDHEADER,
    BLECH_MENGE,
    BLECH_MATERIAL,
    FILENAME
)
AS
SELECT
    a.iddeckelheader,
    sum(a.menge * a.aUSSENblech),
    d1.blech,
    d1.filename
     FROM
      blech.deckel_pos a,
      blech.deckel_description b,
      blech.adm_deckeltypen c,
      blech.v_adm_bleche d1,
      blech.v_adm_bleche d2,
      blech.v_adm_bleche d3,
      blech.v_adm_bleche d4
     WHERE
     b.ID = a.iddescription and
     c.id = a.idDECKELART and
     d1.id = b.a_idblech and
     d2.id = b.i_idblech and
     d3.id = b.i_idblechwanne and
     d4.id = b.i_idblechboden
group by a.iddeckelheader,d1.blech,d1.filename
union
select
    a.iddeckelheader,
    sum(a.menge * a.iNNENblech),
    decode(nvl(a.bodenblech,0),1,d4.blech, decode(nvl(c.wanne,0),1,d3.blech,d2.blech)),
    decode(nvl(a.bodenblech,0),1,d4.filename, decode(nvl(c.wanne,0),1,d3.filename,d2.filename))

      FROM
      blech.deckel_pos a,
      blech.deckel_description b,
      blech.adm_deckeltypen c,
      blech.v_adm_bleche d1,
      blech.v_adm_bleche d2,
      blech.v_adm_bleche d3,
      blech.v_adm_bleche d4
     WHERE
     b.ID = a.iddescription and
     c.id = a.idDECKELART and
     d1.id = b.a_idblech and
     d2.id = b.i_idblech and
     d3.id = b.i_idblechwanne and
     d4.id = b.i_idblechboden
group by a.iddeckelheader,decode(nvl(a.bodenblech,0),1,d4.blech, decode(nvl(c.wanne,0),1,d3.blech,d2.blech)),   decode(nvl(a.bodenblech,0),1,d4.filename, decode(nvl(c.wanne,0),1,d3.filename,d2.filename))