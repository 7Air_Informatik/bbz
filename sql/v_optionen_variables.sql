create or replace view v_optionen_variables
(id, idoption, iddpos, zusatz, ablech_par_x, ablech_par_y, iblech_par_x, iblech_par_y, ablech_abw_xy, 
iblech_abw_x, iblech_abw_y, ablech_par_a, ablech_par_b, iblech_par_a, iblech_par_b, ablech_par_r, iblech_par_r, par_f, par_p, par_u, 
par_v, par_w, par_l, par_k, par_m, par_n, par_t, par_j, par_g, par_h, par_s, par_q, par_o,nennmass_x,nennmass_y)
as
select
   a.ID,
   d.id,
   b.id,
   d.zusatz,
   a.par_x + g.abw_ablech,
   a.par_y + g.abw_ablech,
   a.par_x + g.abw_iblech_x,
   a.par_y + g.abw_iblech_y,
   g.abw_ablech,
   g.abw_iblech_x,
   g.abw_iblech_y,
   b.nennmass_breite_x + (g.abw_ablech * 2),
   b.nennmass_hoehe_y + (g.abw_ablech * 2),
   b.nennmass_breite_x + (g.abw_iblech_x * 2),
   b.nennmass_hoehe_y + (g.abw_iblech_y * 2),
   a.par_r,
   a.par_r + d.diff_innenblech,
   f.par_f,
   decode(nvl(d.par_p,0),0,-1,d.par_p),
   a.par_u,
   a.par_v,
   a.par_w,
   a.par_l,
   a.par_k,
   a.par_m,
   a.par_n,
   c.thermo,
   h.variante,
   blech.pkg_transform.fget_prodtypcode_from_dpos(p_nidpos => b.id),
   a.par_h,
   a.par_s,
   a.par_q,
   a.par_o,
   b.nennmass_breite_x,
   b.nennmass_hoehe_y
from
   blech.deckel_optionen a,
   blech.deckel_pos b,
   blech.deckel_description c,
   blech.adm_optionen d,
   blech.adm_option_steuerung d1,
   blech.v_adm_bleche e1,
   blech.v_adm_bleche e2,
   blech.v_adm_bleche e3,
   blech.v_adm_bleche e4,
   blech.adm_deckeltypen f,
   blech.v_dpos_details g,
   blech.adm_varianten h
where
  b.id = a.iddeckelpos and
  c.id = b.iddescription and
  d.id = a.idoption and
  d1.id = d.id_inc_steuerung and
  e1.ID = c.a_idblech and
  e2.id = c.i_idblech and
  e3.ID = c.i_idblechwanne and
  e4.id = c.i_idblechboden and
  f.id = b.iddeckelart and
  g.iddpos = a.iddeckelpos AND
  h.id = c.idvariante;
