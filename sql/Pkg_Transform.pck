CREATE OR REPLACE PACKAGE Pkg_Transform AS

  FUNCTION Fget_Inc_Code(p_Nidoption     IN NUMBER,
                         p_Ncode         IN NUMBER,
                         p_Stransfercode IN STRING,
                         p_Iblech        IN NUMBER) RETURN STRING;

  FUNCTION Fget_Idprodtyp_From_Dpos(p_Nidpos IN NUMBER) RETURN NUMBER;
  FUNCTION Fget_Prodtypcode_From_Dpos(p_Nidpos IN NUMBER) RETURN NUMBER;

  FUNCTION Fget_Abwicklung(p_Nidptyp       IN NUMBER,
                           p_Nidprodukt    IN NUMBER,
                           p_Nblechstaerke IN NUMBER,
                           p_Nthermo       IN NUMBER,
                           p_Nvalue        IN NUMBER) RETURN NUMBER;

  FUNCTION Fget_Label_Liste(p_Nidoption IN NUMBER, p_Slabel IN STRING)
    RETURN STRING;

  function fget_transfercode(p_nIDParent in number, p_nIDMachine in Number, p_smachine in string)
    return string;
END Pkg_Transform;
/
CREATE OR REPLACE PACKAGE BODY Pkg_Transform AS

  -- ---------------------------------------------------------------
  -- Deklaration allgemeiner Variablen
  v_Par_a NUMBER; -- Zuschnittmass X-Achse
  v_Par_b NUMBER; -- Zuschnittmass Y-Achse
  v_Par_c NUMBER; -- Code f�r Produktionsmaschine
  v_Par_d NUMBER; -- 
  v_Par_e NUMBER; --
  v_Par_f NUMBER; -- Produkt (Deckel, Wanne, Rev-Deckel, etc ....)
  v_Par_g NUMBER; -- Produktionstyp (SKG,SZG,SAG ....)
  v_Par_h NUMBER; -- Horizontaler oder vertikaler Einbau
  v_Par_i NUMBER; -- Innenblech
  v_Par_j NUMBER; -- SD-Variante
  v_Par_k NUMBER; -- 1. Stanzung X-Achse
  v_Par_l NUMBER; -- Anzahl L�cher X-Achse / auf Kreisbogen Anzahl L�scher (z. Bsp. bei Flanschanschluss)
  v_Par_m NUMBER; -- 1. Stanzung Y-Achse
  v_Par_n NUMBER; -- Anzahl L�cher Y-Achse
  v_Par_o NUMBER; --
  v_Par_p NUMBER; -- Stempel
  v_Par_q NUMBER; --
  v_Par_r NUMBER; -- Radius
  v_Par_s NUMBER; -- Auf Kreisbogen Startpunkt 1. Loche im Kreis (in �)
  v_Par_t NUMBER; -- Thermo
  v_Par_u NUMBER; -- Rechteckstanzung: X-Mass
  v_Par_v NUMBER; -- Rechteckstanzung: Y-Mass
  v_Par_w NUMBER; -- Lochabstand (z. Bsp. Lampe, FS-Griff, etc....)
  v_Par_x NUMBER; -- Koordinate X (Mitte)
  v_Par_y NUMBER; -- Koordinate Y (Mitte)
  v_Par_z NUMBER;

  FUNCTION f_Translate(p_Sstring IN STRING) RETURN STRING;
  FUNCTION Fget_Idprodtyp(p_Nidoption IN NUMBER) RETURN NUMBER;

  -- ****************************************************************************************************************

  -- Einstiegsfunktion 
  -- -----------------
  FUNCTION Fget_Inc_Code(p_Nidoption     IN NUMBER,
                         p_Ncode         IN NUMBER,
                         p_Stransfercode IN STRING,
                         p_Iblech        IN NUMBER) RETURN STRING IS
    Sreturn VARCHAR2(250);
  
  BEGIN
    -- Grunddaten ermitteln
  
    IF p_Ncode = 10 -- Loch, Schauglas, runder Ausschnitt
     THEN
      IF p_Iblech = 0 THEN
        SELECT t.Ablech_Par_x,
               t.Ablech_Par_y,
               t.Ablech_Par_a,
               t.Ablech_Par_b,
               0,
               0,
               t.Ablech_Par_r,
               t.Par_p,
               t.Par_f
          INTO v_Par_x,
               v_Par_y,
               v_Par_a,
               v_Par_b,
               v_Par_c,
               v_Par_i,
               v_Par_r,
               v_Par_p,
               v_Par_f
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      ELSE
        SELECT t.Iblech_Par_x,
               t.Iblech_Par_y,
               t.Iblech_Par_a,
               t.Iblech_Par_b,
               0,
               1,
               t.Iblech_Par_r,
               t.Par_p,
               t.Par_f
          INTO v_Par_x,
               v_Par_y,
               v_Par_a,
               v_Par_b,
               v_Par_c,
               v_Par_i,
               v_Par_r,
               v_Par_p,
               v_Par_f
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      END IF;
    
    ELSIF p_Ncode = 11 THEN
      IF p_Iblech = 0 THEN
        SELECT t.Ablech_Par_x,
               t.Ablech_Par_y,
               t.Ablech_Par_a,
               t.Ablech_Par_b,
               0,
               0,
               t.Ablech_Par_r,
               t.Par_o,
               t.Par_f,
               t.Par_h,
               t.Par_s
          INTO v_Par_x,
               v_Par_y,
               v_Par_a,
               v_Par_b,
               v_Par_c,
               v_Par_i,
               v_Par_r,
               v_Par_o,
               v_Par_f,
               v_Par_h,
               v_Par_s
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      ELSE
        SELECT t.Iblech_Par_x,
               t.Iblech_Par_y,
               t.Iblech_Par_a,
               t.Iblech_Par_b,
               0,
               1,
               t.Iblech_Par_r,
               t.Par_o,
               t.Par_f,
               t.Par_h,
               t.Par_s
          INTO v_Par_x,
               v_Par_y,
               v_Par_a,
               v_Par_b,
               v_Par_c,
               v_Par_i,
               v_Par_r,
               v_Par_o,
               v_Par_f,
               v_Par_h,
               v_Par_s
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      END IF;
    
    ELSIF p_Ncode = 15 THEN
      -- Deckelverbohrung (Serie)
      IF p_Iblech = 0 THEN
        SELECT t.Ablech_Par_a,
               t.Ablech_Par_b,
               t.Par_f,
               t.Par_g,
               p_Iblech,
               t.Par_t,
               t.Par_l,
               t.Par_n
          INTO v_Par_a,
               v_Par_b,
               v_Par_f,
               v_Par_g,
               v_Par_i,
               v_Par_t,
               v_Par_l,
               v_Par_n
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      END IF;
    
    ELSIF p_Ncode = 16 THEN
      -- Deckelverbohrung (Einzel)
      IF p_Iblech = 0 THEN
        SELECT t.Ablech_Par_a,
               t.Ablech_Par_b,
               t.Par_f,
               t.Par_g,
               t.Par_h,
               t.Par_t,
               p_Iblech,
               t.Nennmass_x,
               t.Nennmass_y,
               t.Par_x
          INTO v_Par_a,
               v_Par_b,
               v_Par_f,
               v_Par_g,
               v_Par_h,
               v_Par_t,
               v_Par_i,
               v_Par_u,
               v_Par_v,
               v_Par_x
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      END IF;
    
    ELSIF p_Ncode = 17 THEN
      -- Schaumloch einzel
      IF p_Iblech = 0 THEN
        SELECT t.Ablech_Par_a,
               t.Ablech_Par_b,
               t.Par_h,
               0,
               t.Par_j,
               t.Nennmass_x,
               t.Nennmass_y,
               t.Par_x
          INTO v_Par_a,
               v_Par_b,
               v_Par_h,
               v_Par_i,
               v_Par_j,
               v_Par_u,
               v_Par_v,
               v_Par_x
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      ELSE
        SELECT t.Iblech_Par_a,
               t.Iblech_Par_b,
               t.Par_h,
               1,
               t.Par_j,
               t.Nennmass_x,
               t.Nennmass_y,
               t.Par_x
          INTO v_Par_a,
               v_Par_b,
               v_Par_h,
               v_Par_i,
               v_Par_j,
               v_Par_u,
               v_Par_v,
               v_Par_x
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      END IF;
    
    ELSIF p_Ncode = 20 THEN
      -- Rechteckiger Ausschnitt
      IF p_Iblech = 0 THEN
        SELECT t.Ablech_Par_a,
               t.Ablech_Par_b,
               t.Par_f,
               0,
               t.Par_u,
               t.Par_v,
               t.Ablech_Par_x,
               t.Ablech_Par_y
          INTO v_Par_a,
               v_Par_b,
               v_Par_f,
               v_Par_i,
               v_Par_u,
               v_Par_v,
               v_Par_x,
               v_Par_y
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      ELSE
        SELECT t.Iblech_Par_a,
               t.Iblech_Par_b,
               t.Par_f,
               1,
               t.Par_u,
               t.Par_v,
               t.Iblech_Par_x,
               t.Iblech_Par_y
          INTO v_Par_a,
               v_Par_b,
               v_Par_f,
               v_Par_i,
               v_Par_u,
               v_Par_v,
               v_Par_x,
               v_Par_y
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      END IF;
    
    ELSIF p_Ncode = 21 THEN
      -- Rechteckiger Ausschnitt mit Radius
      IF p_Iblech = 0 THEN
        SELECT t.Ablech_Par_a,
               t.Ablech_Par_b,
               t.Par_f,
               0,
               t.Par_r,
               t.Par_u,
               t.Par_v,
               t.Ablech_Par_x,
               t.Ablech_Par_y
          INTO v_Par_a,
               v_Par_b,
               v_Par_f,
               v_Par_i,
               v_Par_r,
               v_Par_u,
               v_Par_v,
               v_Par_x,
               v_Par_y
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      ELSE
        SELECT t.Iblech_Par_a,
               t.Iblech_Par_b,
               t.Par_f,
               1,
               t.Par_r,
               t.Par_u,
               t.Par_v,
               t.Iblech_Par_x,
               t.Iblech_Par_y
          INTO v_Par_a,
               v_Par_b,
               v_Par_f,
               v_Par_i,
               v_Par_r,
               v_Par_u,
               v_Par_v,
               v_Par_x,
               v_Par_y
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      END IF;
    
    ELSIF p_Ncode = 30 THEN
      -- Rahmenverbohrung
      IF p_Iblech = 0 THEN
        SELECT t.Ablech_Par_a,
               t.Ablech_Par_b,
               t.Ablech_Par_x,
               t.Ablech_Par_y,
               t.Par_f,
               0,
               t.Par_k,
               t.Par_l,
               t.Par_m,
               t.Par_n,
               t.Par_p,
               t.Par_u,
               t.Par_v
          INTO v_Par_a,
               v_Par_b,
               v_Par_x,
               v_Par_y,
               v_Par_f,
               v_Par_i,
               v_Par_k,
               v_Par_l,
               v_Par_m,
               v_Par_n,
               v_Par_p,
               v_Par_u,
               v_Par_v
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      ELSE
        SELECT t.Iblech_Par_a,
               t.Iblech_Par_b,
               t.Iblech_Par_x,
               t.Iblech_Par_y,
               t.Par_f,
               1,
               t.Par_k,
               t.Par_l,
               t.Par_m,
               t.Par_n,
               t.Par_p,
               t.Par_u,
               t.Par_v
          INTO v_Par_a,
               v_Par_b,
               v_Par_x,
               v_Par_y,
               v_Par_f,
               v_Par_i,
               v_Par_k,
               v_Par_l,
               v_Par_m,
               v_Par_n,
               v_Par_p,
               v_Par_u,
               v_Par_v
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      END IF;
    
    ELSIF p_Ncode = 31 THEN
      -- Winkelrahmen bei Sarnafil
      IF p_Iblech = 0 THEN
        SELECT t.Ablech_Par_a,
               t.Ablech_Par_b,
               t.Ablech_Par_x,
               t.Ablech_Par_y,
               0,
               t.Par_o,
               t.Par_u,
               t.Par_v
          INTO v_Par_a,
               v_Par_b,
               v_Par_x,
               v_Par_y,
               v_Par_i,
               v_Par_o,
               v_Par_u,
               v_Par_v
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      ELSE
        SELECT t.Iblech_Par_a,
               t.Iblech_Par_b,
               t.Iblech_Par_x,
               t.Iblech_Par_y,
               1,
               t.Par_o,
               t.Par_u,
               t.Par_v
          INTO v_Par_a,
               v_Par_b,
               v_Par_x,
               v_Par_y,
               v_Par_i,
               v_Par_o,
               v_Par_u,
               v_Par_v
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      END IF;
    
    ELSIF p_Ncode = 32 THEN
      -- Klappenverbohrung
      IF p_Iblech = 0 THEN
        SELECT t.Ablech_Par_a,
               t.Ablech_Par_b,
               t.Ablech_Par_x,
               t.Ablech_Par_y,
               0,
               t.Par_u,
               t.Par_v
          INTO v_Par_a,
               v_Par_b,
               v_Par_x,
               v_Par_y,
               v_Par_i,
               v_Par_u,
               v_Par_v
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      ELSE
        SELECT t.Iblech_Par_a,
               t.Iblech_Par_b,
               t.Iblech_Par_x,
               t.Iblech_Par_y,
               1,
               t.Par_u,
               t.Par_v
          INTO v_Par_a,
               v_Par_b,
               v_Par_x,
               v_Par_y,
               v_Par_i,
               v_Par_u,
               v_Par_v
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      END IF;
    
    ELSIF p_Ncode = 40 THEN
      -- Ventieinbau
      IF p_Iblech = 0 THEN
        SELECT t.Ablech_Par_a,
               t.Ablech_Par_b,
               t.Ablech_Par_x,
               t.Ablech_Par_y,
               t.Par_f,
               t.Par_h,
               0,
               t.Par_u,
               t.Par_v
          INTO v_Par_a,
               v_Par_b,
               v_Par_x,
               v_Par_y,
               v_Par_f,
               v_Par_h,
               v_Par_i,
               v_Par_u,
               v_Par_v
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      ELSE
        SELECT t.Iblech_Par_a,
               t.Iblech_Par_b,
               t.Iblech_Par_x,
               t.Iblech_Par_y,
               t.Par_f,
               t.Par_h,
               1,
               t.Par_u,
               t.Par_v
          INTO v_Par_a,
               v_Par_b,
               v_Par_x,
               v_Par_y,
               v_Par_f,
               v_Par_h,
               v_Par_i,
               v_Par_u,
               v_Par_v
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      END IF;
    
    ELSIF p_Ncode = 70 THEN
      -- Flanschanschl�sse
      IF p_Iblech = 0 THEN
        SELECT t.Ablech_Par_a,
               t.Ablech_Par_b,
               t.Ablech_Par_x,
               t.Ablech_Par_y,
               t.Par_f,
               0,
               t.Par_l,
               t.Par_p,
               t.Ablech_Par_r,
               t.Par_s,
               t.Par_q,
               t.Par_o
          INTO v_Par_a,
               v_Par_b,
               v_Par_x,
               v_Par_y,
               v_Par_f,
               v_Par_i,
               v_Par_l,
               v_Par_p,
               v_Par_r,
               v_Par_s,
               v_Par_q,
               v_Par_o
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      ELSE
        SELECT t.Iblech_Par_a,
               t.Iblech_Par_b,
               t.Iblech_Par_x,
               t.Iblech_Par_y,
               t.Par_f,
               1,
               t.Par_l,
               t.Par_p,
               t.Iblech_Par_r,
               t.Par_s,
               t.Par_q
          INTO v_Par_a,
               v_Par_b,
               v_Par_x,
               v_Par_y,
               v_Par_f,
               v_Par_i,
               v_Par_l,
               v_Par_p,
               v_Par_r,
               v_Par_s,
               v_Par_q
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      END IF;
    
    ELSIF p_Ncode = 100 THEN
      -- FS-Griff (nur auf Aussenblech m�glich)
      IF p_Iblech = 0 THEN
        SELECT t.Ablech_Par_a,
               t.Ablech_Par_b,
               t.Ablech_Par_x,
               t.Ablech_Par_y,
               t.Par_h
          INTO v_Par_a, v_Par_b, v_Par_x, v_Par_y, v_Par_h
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      ELSE
        NULL;
      END IF;
    
    ELSIF p_Ncode = 101 THEN
      -- Lampe (nur auf Innenblech m�glich)
      IF p_Iblech = 0 THEN
        NULL;
      ELSE
        SELECT t.Iblech_Par_x, t.Iblech_Par_y, t.Par_w, t.Par_h
          INTO v_Par_x, v_Par_y, v_Par_w, v_Par_h
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      END IF;
    
    ELSIF p_Ncode = 110 THEN
      -- einzelner Verschluss
      IF p_Iblech = 0 THEN
        SELECT t.Ablech_Par_a,
               t.Ablech_Par_b,
               t.Par_x,
               t.Par_g,
               t.Par_h,
               0,
               t.Nennmass_x,
               t.Nennmass_y
          INTO v_Par_a,
               v_Par_b,
               v_Par_x,
               v_Par_g,
               v_Par_h,
               v_Par_i,
               v_Par_u,
               v_Par_v
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      ELSE
        SELECT t.Iblech_Par_a,
               t.Iblech_Par_b,
               t.Par_x,
               t.Par_g,
               t.Par_h,
               1,
               t.Nennmass_x,
               t.Nennmass_y
          INTO v_Par_a,
               v_Par_b,
               v_Par_x,
               v_Par_g,
               v_Par_h,
               v_Par_i,
               v_Par_u,
               v_Par_v
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      END IF;
    
    ELSIF p_Ncode = 112 THEN
      -- Scharnier einzel
      IF p_Iblech = 0 THEN
        SELECT t.Ablech_Par_a,
               t.Ablech_Par_b,
               t.Par_g,
               t.Par_h,
               t.Nennmass_x,
               t.Nennmass_y,
               t.Par_x
          INTO v_Par_a,
               v_Par_b,
               v_Par_g,
               v_Par_h,
               v_Par_u,
               v_Par_v,
               v_Par_x
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      END IF;
    
    ELSIF p_Ncode = 500 THEN
      -- Ablauf seitlich (Deckelwanne)
      IF p_Iblech = 0 THEN
        SELECT t.Ablech_Par_a, t.Ablech_Par_x, 0, t.Par_g
          INTO v_Par_a, v_Par_x, v_Par_i, v_Par_g
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      
      ELSE
        SELECT t.Iblech_Par_a, t.Iblech_Par_x, 1, t.Par_g
          INTO v_Par_a, v_Par_x, v_Par_i, v_Par_g
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      
      END IF;
    
    ELSIF p_Ncode = 501 THEN
      -- Ablauf unten (Deckelwanne)
      IF p_Iblech = 0 THEN
        SELECT t.Ablech_Par_a, t.Ablech_Par_x, 0, t.Par_g
          INTO v_Par_a, v_Par_x, v_Par_i, v_Par_g
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      ELSE
        SELECT t.Iblech_Par_a, t.Iblech_Par_x, 1, t.Par_g
          INTO v_Par_a, v_Par_x, v_Par_i, v_Par_g
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      END IF;
    
    ELSIF p_Ncode = 510 THEN
      -- Ablauf seitlich (VDI-Wanne)
      IF p_Iblech = 0 THEN
        SELECT t.Ablech_Par_a, t.Ablech_Par_x, 0, t.Par_t, t.Par_j, t.Par_g
          INTO v_Par_a, v_Par_x, v_Par_i, v_Par_t, v_Par_j, v_Par_g
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      ELSE
        SELECT t.Iblech_Par_a, t.Iblech_Par_x, 1, t.Par_t, t.Par_j, t.Par_g
          INTO v_Par_a, v_Par_x, v_Par_i, v_Par_t, v_Par_j, v_Par_g
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      END IF;
    
    ELSIF p_Ncode = 511 THEN
      -- Ablauf unten (VDI-Wanne)
      IF p_Iblech = 0 THEN
        SELECT t.Ablech_Par_a, t.Ablech_Par_x, 0, t.Par_t, t.Par_j, t.Par_g
          INTO v_Par_a, v_Par_x, v_Par_i, v_Par_t, v_Par_j, v_Par_g
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      ELSE
        SELECT t.Iblech_Par_a, t.Iblech_Par_x, 1, t.Par_t, t.Par_j, t.Par_g
          INTO v_Par_a, v_Par_x, v_Par_i, v_Par_t, v_Par_j, v_Par_g
          FROM Blech.v_Optionen_Variables t
         WHERE t.Id = p_Nidoption;
      END IF;
    
    END IF;
    Sreturn := f_Translate(p_Stransfercode);
    RETURN Sreturn;
  
  END Fget_Inc_Code;

  -- ---------------------------------------------------------------

  -- �bersetzen des "INC-Strings"
  FUNCTION f_Translate(p_Sstring IN STRING) RETURN STRING IS
    Sreturn VARCHAR(250 CHAR);
  
  BEGIN
    -- Initialisierung von "sReturn"
    Sreturn := p_Sstring;
  
    -- Zuordnung der Werte
    Sreturn := REPLACE(Sreturn, '#a', 'a' || v_Par_a);
    Sreturn := REPLACE(Sreturn, '#b', 'b' || v_Par_b);
    Sreturn := REPLACE(Sreturn, '#c', 'c' || v_Par_c);
    Sreturn := REPLACE(Sreturn, '#d', 'd' || v_Par_d);
    Sreturn := REPLACE(Sreturn, '#e', 'e' || v_Par_e);
    Sreturn := REPLACE(Sreturn, '#f', 'f' || v_Par_f);
    Sreturn := REPLACE(Sreturn, '#g', 'g' || v_Par_g);
    Sreturn := REPLACE(Sreturn, '#h', 'h' || v_Par_h);
    Sreturn := REPLACE(Sreturn, '#i', 'i' || v_Par_i);
    Sreturn := REPLACE(Sreturn, '#j', 'j' || v_Par_j);
    Sreturn := REPLACE(Sreturn, '#k', 'k' || v_Par_k);
    Sreturn := REPLACE(Sreturn, '#l', 'l' || v_Par_l);
    Sreturn := REPLACE(Sreturn, '#m', 'm' || v_Par_m);
    Sreturn := REPLACE(Sreturn, '#n', 'n' || v_Par_n);
    Sreturn := REPLACE(Sreturn, '#o', 'o' || v_Par_o);
    Sreturn := REPLACE(Sreturn, '#p', 'p' || v_Par_p);
    Sreturn := REPLACE(Sreturn, '#q', 'q' || v_Par_q);
    Sreturn := REPLACE(Sreturn, '#r', 'r' || v_Par_r);
    Sreturn := REPLACE(Sreturn, '#s', 's' || v_Par_s);
    Sreturn := REPLACE(Sreturn, '#t', 't' || v_Par_t);
    Sreturn := REPLACE(Sreturn, '#u', 'u' || v_Par_u);
    Sreturn := REPLACE(Sreturn, '#v', 'v' || v_Par_v);
    Sreturn := REPLACE(Sreturn, '#w', 'w' || v_Par_w);
    Sreturn := REPLACE(Sreturn, '#x', 'x' || v_Par_x);
    Sreturn := REPLACE(Sreturn, '#y', 'y' || v_Par_y);
    Sreturn := REPLACE(Sreturn, '#z', 'z' || v_Par_z);
  
    RETURN Sreturn;
  
  END;
  --  ------------------------------------------------------------


  -- --------------------------------------------------------------------
  -- ID-Produktionstyp ermitteln (von Deckelposition)
  FUNCTION Fget_Idprodtyp_From_Dpos(p_Nidpos IN NUMBER) RETURN NUMBER IS
    Nreturn NUMBER;
  
    v_Nidtermin NUMBER;
  
    CURSOR Cur_Idtermin IS
      SELECT t.Idtermin
        FROM Blech.Deckel_Header t
       WHERE t.Id = (SELECT p.Iddeckelheader
                       FROM Blech.Deckel_Pos p
                      WHERE p.Id = p_Nidpos);
  
    CURSOR Cur_Idptyp IS
      SELECT t.Idprodtyp
        FROM Blech.Adm_Gtyp_Ptyp t
       WHERE t.Idgertyp =
             (SELECT p.t_Seqnr
                FROM Sevprod.a_Pos_Allg p
               WHERE p.p_Seqnr =
                     (SELECT b.p_Seqnr
                        FROM Sevprod.a_Termin b
                       WHERE b.At_Seqnr = v_Nidtermin));
  
  BEGIN
    OPEN Cur_Idtermin;
    FETCH Cur_Idtermin
      INTO v_Nidtermin;
    CLOSE Cur_Idtermin;
  
    OPEN Cur_Idptyp;
    FETCH Cur_Idptyp
      INTO Nreturn;
    CLOSE Cur_Idptyp;
  
    RETURN Nreturn;
  
  END Fget_Idprodtyp_From_Dpos;

  -- Verarbeitungsparameter desProduktionstyp ermitteln (von Deckelposition)
  FUNCTION Fget_Prodtypcode_From_Dpos(p_Nidpos IN NUMBER) RETURN NUMBER IS
    Nreturn NUMBER;
  
    v_Nidtermin NUMBER;
    v_Nidgtyp   NUMBER;
  
    CURSOR Cur_Idtermin IS
      SELECT t.Idtermin
        FROM Blech.Deckel_Header t
       WHERE t.Id = (SELECT p.Iddeckelheader
                       FROM Blech.Deckel_Pos p
                      WHERE p.Id = p_Nidpos);
  
    CURSOR Cur_Idptyp IS
      SELECT t.Idprodtyp
        FROM Blech.Adm_Gtyp_Ptyp t
       WHERE t.Idgertyp =
             (SELECT p.t_Seqnr
                FROM Sevprod.a_Pos_Allg p
               WHERE p.p_Seqnr =
                     (SELECT b.p_Seqnr
                        FROM Sevprod.a_Termin b
                       WHERE b.At_Seqnr = v_Nidtermin));
  
    CURSOR Cur_Ptyp_Code IS
      SELECT t.Id_Inc_Steuerung
        FROM Blech.Adm_Produktionstypen t
       WHERE t.Id = v_Nidgtyp;
  
  BEGIN
    OPEN Cur_Idtermin;
    FETCH Cur_Idtermin
      INTO v_Nidtermin;
    CLOSE Cur_Idtermin;
  
    OPEN Cur_Idptyp;
    FETCH Cur_Idptyp
      INTO v_Nidgtyp;
    CLOSE Cur_Idptyp;
  
    OPEN Cur_Ptyp_Code;
    FETCH Cur_Ptyp_Code
      INTO Nreturn;
    CLOSE Cur_Ptyp_Code;
  
    RETURN Nreturn;
  
  END Fget_Prodtypcode_From_Dpos;
  -- --------------------------------------------------------------------

  -- Abwicklung ermitteln
  FUNCTION Fget_Abwicklung(p_Nidptyp       IN NUMBER,
                           p_Nidprodukt    IN NUMBER,
                           p_Nblechstaerke IN NUMBER,
                           p_Nthermo       IN NUMBER,
                           p_Nvalue        IN NUMBER) RETURN NUMBER IS
    Nreturn NUMBER;
  
    v_Nadiffnennmass   NUMBER;
    v_Nidiffnennmass   NUMBER;
    v_Nabwaussenblech  NUMBER;
    v_Nabwinnenblech_x NUMBER;
    v_Nabwinnenblech_y NUMBER;
  
    CURSOR Cur_Abwicklung IS
      SELECT t.a_Diff_Nennmass,
             t.i_Diff_Nennmass,
             t.Abw_Aussenblech,
             t.Abw_Innenblech_x,
             t.Abw_Innenblech_y
        FROM Blech.Adm_Abwicklungen t
       WHERE t.Idprodtyp = p_Nidptyp
         AND t.Iddeckeltyp = p_Nidprodukt
         AND t.Blechstaerke = p_Nblechstaerke
         AND t.Thermo = p_Nthermo;
  BEGIN
  
    OPEN Cur_Abwicklung;
    FETCH Cur_Abwicklung
      INTO v_Nadiffnennmass,
           v_Nidiffnennmass,
           v_Nabwaussenblech,
           v_Nabwinnenblech_x,
           v_Nabwinnenblech_y;
    CLOSE Cur_Abwicklung;
  
    /*
          p_nValue
          1 = Abwicklung Aussenblech / 2
          2 = Abwicklung Innenblech_X / 2
          3 = Abwicklung Innenblech_Y / 2
    */
  
    IF p_Nvalue = 1 THEN
      Nreturn := (v_Nabwaussenblech + v_Nadiffnennmass) / 2;
    ELSIF p_Nvalue = 2 THEN
      Nreturn := (v_Nabwinnenblech_x + v_Nidiffnennmass) / 2;
    ELSE
      Nreturn := (v_Nabwinnenblech_y + v_Nidiffnennmass) / 2;
    END IF;
    RETURN Nreturn;
  END Fget_Abwicklung;

  -- --------------------------------------------------------------------

  -- Produktionstyp ermitteln (Von  Option)
  FUNCTION Fget_Idprodtyp(p_Nidoption IN NUMBER) RETURN NUMBER IS
    Nreturn NUMBER;
  
    v_Nidtermin NUMBER;
  
    CURSOR Cur_Idtermin IS
      SELECT t.Idtermin
        FROM Blech.Deckel_Header t
       WHERE t.Id = (SELECT p.Iddeckelheader
                       FROM Blech.Deckel_Pos p
                      WHERE p.Id = (SELECT o.Iddeckelpos
                                      FROM Blech.Deckel_Optionen o
                                     WHERE o.Id = p_Nidoption));
  
    CURSOR Cur_Idptyp IS
      SELECT t.Idprodtyp
        FROM Blech.Adm_Gtyp_Ptyp t
       WHERE t.Idgertyp =
             (SELECT p.t_Seqnr
                FROM Sevprod.a_Pos_Allg p
               WHERE p.p_Seqnr =
                     (SELECT b.p_Seqnr
                        FROM Sevprod.a_Termin b
                       WHERE b.At_Seqnr = v_Nidtermin));
  
  BEGIN
    OPEN Cur_Idtermin;
    FETCH Cur_Idtermin
      INTO v_Nidtermin;
    CLOSE Cur_Idtermin;
  
    OPEN Cur_Idptyp;
    FETCH Cur_Idptyp
      INTO Nreturn;
    CLOSE Cur_Idptyp;
  
    RETURN Nreturn;
  
  END Fget_Idprodtyp;

  -- --------------------------------------------------------------------

  FUNCTION Fget_Label_Liste(p_Nidoption IN NUMBER, p_Slabel IN STRING)
    RETURN STRING IS
    Sreturn VARCHAR2(200 CHAR);
    CURSOR Cur_Optionen IS
      SELECT t.Par_h,
             t.Par_k,
             t.Par_l,
             t.Par_m,
             t.Par_n,
             t.Par_o,
             t.Par_q,
             t.Par_r,
             t.Par_s,
             t.Par_u,
             t.Par_v,
             t.Par_x,
             t.Par_y
        FROM Blech.Deckel_Optionen t
       WHERE t.Id = p_Nidoption;
  
  BEGIN
    OPEN Cur_Optionen;
    FETCH Cur_Optionen
      INTO v_Par_h,
           v_Par_k,
           v_Par_l,
           v_Par_m,
           v_Par_n,
           v_Par_o,
           v_Par_q,
           v_Par_r,
           v_Par_s,
           v_Par_u,
           v_Par_v,
           v_Par_x,
           v_Par_y;
    CLOSE Cur_Optionen;
  
    -- Zuordnung der Werte
    Sreturn := p_Slabel;
    Sreturn := REPLACE(Sreturn, '#h', v_Par_h);
    Sreturn := REPLACE(Sreturn, '#k', v_Par_k);
    Sreturn := REPLACE(Sreturn, '#l', v_Par_l);
    Sreturn := REPLACE(Sreturn, '#m', v_Par_m);
    Sreturn := REPLACE(Sreturn, '#n', v_Par_n);
    Sreturn := REPLACE(Sreturn, '#o', v_Par_o);
    Sreturn := REPLACE(Sreturn, '#q', v_Par_q);
    Sreturn := REPLACE(Sreturn, '#r', v_Par_r);
    Sreturn := REPLACE(Sreturn, '#s', v_Par_s);
    Sreturn := REPLACE(Sreturn, '#u', v_Par_u);
    Sreturn := REPLACE(Sreturn, '#v', v_Par_v);
    Sreturn := REPLACE(Sreturn, '#x', v_Par_x);
    Sreturn := REPLACE(Sreturn, '#y', v_Par_y);
  
    RETURN Sreturn;
  
  END Fget_Label_Liste;
  -- --------------------------------------------------------------------

  function fget_transfercode(p_nIDParent in number, p_nidmachine in number, p_smachine in string)
    return string is
    sReturn varchar2(50 char);
		v_nidmachine number;
		
		cursor cur_machine is
		select t.id
		from blech.adm_blechcenter t
		where t.blechcenter = p_smachine;
		
    cursor cur_code is
      select t.transfercode
        from blech.adm_transfercode t
       where t.idparent = p_nidparent
         and t.idmachine = v_nidmachine;
  
  begin
    
	  if nvl(p_nidmachine,0) = 0 then
			open cur_machine;
			fetch cur_machine into v_nidmachine;
			close cur_machine;
		else
			v_nidmachine := p_nidmachine;
		end if;
		
		open cur_code;
    fetch cur_code
      into sReturn;
    close cur_code;
    return sReturn;
		
  end fget_transfercode;

-- ----------------------------------------------------------------------------                  

END Pkg_Transform;
/
