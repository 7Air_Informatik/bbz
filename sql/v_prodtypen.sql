create or replace view blech.v_prodtypen
(
idtermin,
idprodtyp
)
as select
  a.at_seqnr,
  c.idprodtyp
from
  sevprod.a_termin a,
  sevprod.a_pos_allg b,
  blech.adm_gtyp_ptyp c
where
  b.p_seqnr = a.p_seqnr and
  c.idgertyp = b.t_seqnr

