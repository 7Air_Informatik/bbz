CREATE OR REPLACE VIEW SEVPROD.VI_BBZ_ETIKETTEN
(
    AUSSEN,
    SEQNR,
    REFNR_POS,
    ABLECH,
    IBLECH,
    DIMENSION
)
AS
SELECT
1,
 DP.SEQNR,
 ap.refnr || '.' || ap.pos,
 trim(f_getcolor(DPQ.a_farbetext)||decode(dpq.a_schicht,null,null,' '||dpq.a_schicht||'� ')),
 trim(decode(dp.boden,1,'V2A',decode(dpq.i_material,1,'V2A.',2,'V4A.',0,decode(dpq.i_farbetext,'','SVZ', NULL)))||decode(dp.boden,1,null,f_getcolor(DPQ.i_farbetext))),
 dp.breite || ' x ' || dp.hoehe
FROM
 SEVPROD.DECK_POS DP,
 SEVPROD.DECK_KOPF DK,
 SEVPROD.A_TERMIN AT,
 SEVPROD.A_POS_ALLG AP,
 SEVPROD.DECK_POS_Q DPQ
WHERE
  DP.BBZ != 1 AND
 DK.SEQNR = DP.DK_SEQNR AND
 DPQ.SEQNR = DP.Q_SEQNR AND
 AT.AT_SEQNR = DK.AT_SEQNR AND
 AP.P_SEQNR = AT.P_SEQNR
UNION
SELECT
0,
 DP.SEQNR,
 ap.refnr || '.' || ap.pos,
NULL,
 trim(decode(dp.boden,1,'V2A',decode(dpq.i_material,1,'V2A.',2,'V4A.',0,decode(dpq.i_farbetext,'','SVZ', NULL)))||decode(dp.boden,1,null,f_getcolor(DPQ.i_farbetext))),
 dp.breite || ' x ' || dp.hoehe
FROM
 SEVPROD.DECK_POS DP,
 SEVPROD.DECK_KOPF DK,
 SEVPROD.A_TERMIN AT,
 SEVPROD.A_POS_ALLG AP,
 SEVPROD.DECK_POS_Q DPQ
WHERE
  DP.BBZ != 1 AND
 DK.SEQNR = DP.DK_SEQNR AND
 DPQ.SEQNR = DP.Q_SEQNR AND
 AT.AT_SEQNR = DK.AT_SEQNR AND
 AP.P_SEQNR = AT.P_SEQNR